def api_list_presentations(request, conference_id):
    presentations = [
        {
            "title": p.title,
            "status": p.status.name,
            "href": p.get_api_url(),
        }
        for p in Presentation.objects.filter(conference=conference_id)
    ]
    # return JsonResponse({"presentations": presentations})
    return JsonResponse(
        presentations,
        encoder=PresentationListEncoder,
        safe=False,
    )
